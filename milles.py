from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import time
import glob
import os
from csv import DictReader
import csv
import mysql.connector

# Start chrome scrapping
def run_chrome(addr):
    driver.get("https://www.google.com/maps?saddr=IP100DD&daddr="+addr)
    #time.sleep(2)
    try:
        try:
            btn = driver.find_element_by_xpath('/html/body/div/c-wiz/div/div/div/div[2]/div[1]/div[4]/form/div[1]/div/button')
            btn.click()
            time.sleep(2)
        except:
            btn = driver.find_element_by_xpath('//*[@id="introAgreeButton"]')
            btn.click()
            time.sleep(2)
    except:
        pass

# GET ETAs from google by scrapping
def request_data(jobs):
    remake = []
    for job in jobs:
        run_chrome(job[2])
        eta = get_eta()
        job = list(job)
        job.append(eta)
        remake.append(job)
    return remake


# GET ETA by scrapping.
def get_eta():
    try:
        eta_distance = driver.find_element_by_xpath('//*[@id="section-directions-trip-0"]/div/div[1]/div[1]/div[2]/div').text
    except:
        eta_distance = driver.find_element_by_xpath('//*[@id="section-directions-trip-0"]/div/div[3]/div[1]/div[2]').text
    return eta_distance

def find_file():
    list_of_files = glob.glob('C:/Users/4844/Downloads/*.csv')
    latest_file = max(list_of_files, key=os.path.getctime)
    print("Am gasit fisierul")
    return latest_file

def read_csv(file):
    with open(file, 'r') as read_obj:
        csv_dict_reader = DictReader(read_obj)
        array = []
        for row in csv_dict_reader:
            name = row['Resource'].split()
            nr = len(name)
            if nr > 2:
                indice = name[0][0] + name[1]
            else:
                indice = name[0][0] + name[0][4] + name[0][5]
            array.append([indice,row['Stop 2'],row['Stop 2 Postcode']])
        print("Am citit csv-ul")
        return array

def write_into_csv(data):
    with open('miles.csv', mode='w') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for driver in data:
            employee_writer.writerow([driver[0],driver[1],driver[3]])

chrome_options = Options();
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument("--headless")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument("--lang=en-GB")
chrome_options.add_experimental_option("excludeSwitches", ['enable-logging']);
driver = webdriver.Chrome(ChromeDriverManager().install(),options=chrome_options)
# Start Scrapping & Updateing db.
file = find_file()
data = read_csv(file)
data = request_data(data)
write_into_csv(data)
print(data)
print("Gata")
driver.close()
driver.quit()
