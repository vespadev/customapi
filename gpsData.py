import requests
import json
import mysql.connector

api_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NjU4NTAsImlhdCI6MTY0Nzg3MzcwMywiZXhwIjoxNjc5NDA5NzAzfQ.z8Kv6RTtoHOCT5aX_0_ac19Xy5n4QLFLtnmhs_INlDQ"
params = {
    "accept": "application/json",
    "Authorization": 'Bearer ' + api_key
}

def getDriveTime(imei):
    resp = requests.get("https://api.gpslive.app/v1/devices/"+imei+"/today-overviews", headers=params)
    device = resp.json()
    try:
        return device['drives_duration']
    except:
        return "No driver today"

# Basic info for each device
def getDevices():
    resp = requests.get("https://api.gpslive.app/v1/devices", headers=params)
    devices = resp.json()
    truck_dict = {}
    trucks = []
    for device in devices:
        if device['plateNumber'] == "1TO1":
            drive_time = getDriveTime(device['imei'])
            truck_dict = {
                "imei": device['imei'],
                "name": device['name'],
                "plateNumber": device['plateNumber'],
                "status": device['objectData']['status'],
                "addr": device['objectData']['data']['address'],
                "lng": device['objectData']['data']['longitude'],
                "lat": device['objectData']['data']['latitude'],
                "drive_time": drive_time
            }
            trucks.append(truck_dict)
    return trucks

def update_trucks(trucks):
    mydb = mysql.connector.connect(
        host="89.46.7.25",
        user="vespadev_root",
        password="Pinguin1..",
        database="vespadev_deeatvx"
    )
    mycursor = mydb.cursor()
    for truck in trucks:
        mycursor = mydb.cursor()
        sql = "UPDATE trucks SET current_address= %s ,drive_time = %s ,status = %s ,lat= %s ,lng = %s WHERE serial_truck = %s"
        params = (truck['addr'],truck['drive_time'],truck['status'],truck['lat'],truck['lng'],truck['imei'])
        mycursor.execute(sql,params)
        mydb.commit()

trucks = getDevices()
update_trucks(trucks)
