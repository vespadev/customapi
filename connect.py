from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time
import glob
import os
from csv import DictReader
import mysql.connector

def download_jobs():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_experimental_option("excludeSwitches", ['enable-logging'])
    driver = webdriver.Chrome(ChromeDriverManager().install(),options=chrome_options)
    driver.get("https://1to1transport.xt.truckcom.net/Home/Jobs")
    time.sleep(1)
    email = driver.find_element_by_name('Username')
    email.send_keys("deea")
    passw = driver.find_element_by_name('Password')
    passw.send_keys("welcome1")
    submit = driver.find_element_by_id("LoginButton")
    submit.click()
    time.sleep(2)
    get_div = driver.find_element_by_id('JobsGrid_NextButton')
    get_div.click()
    time.sleep(1)
    btn0 = driver.find_element_by_xpath("/html/body/div[17]/table/tbody/tr[2]/td/div[1]/table/tbody/tr/td/div/div/div[5]/div/table/tbody/tr[1]/td[5]")
    btn0.click()
    btn1 = driver.find_element_by_xpath("//*[@id='JobsGrid']/div/div[4]/div/div/div[3]/div[4]/div/div")
    btn1.click()
    btn2 = driver.find_element_by_xpath("/html/body/div[25]/div/div/ul/li[2]/div")
    btn2.click()
    btn3 = driver.find_element_by_xpath("/html/body/div[25]/div/div/ul/li[2]/div/div[2]/ul/li[1]/div")
    btn3.click()
    print("Am terminat descarcarea")
    time.sleep(1)
    driver.close()

def find_file():
    list_of_files = glob.glob('C:/Users/4844/Downloads/*.csv')
    latest_file = max(list_of_files, key=os.path.getctime)
    print("Am gasit fisierul")
    return latest_file

def read_csv(file):
    with open(file, 'r') as read_obj:
        csv_dict_reader = DictReader(read_obj)
        array = []
        last_name = ""
        for row in csv_dict_reader:
            name = row['Resource'].split('-')
            if name[0][-2].isnumeric() :
                indice = "D" + name[0][-2] + name[0][-1]
            else:
                indice = "D" + name[0][-1]
            array.append([indice,row['Stop 2 Postcode']])
        print("Am citit csv-ul")
        print(array)
        return array

def get_truck(name):
    mydb = mysql.connector.connect(
      host="hosting2166736.online.pro",
      user="00461784_deeatvx",
      password="Pinguin1..",
      database="00461784_deeatvx"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT id FROM indici WHERE indice='"+name+"'")
    myresult = mycursor.fetchone()
    return myresult[0]

def update_db(array):
    mydb = mysql.connector.connect(
      host="hosting2166736.online.pro",
      user="00461784_deeatvx",
      password="Pinguin1..",
      database="00461784_deeatvx"
    )
    mycursor = mydb.cursor()
    for truck in array:
        id = get_truck(truck[0])
        sql = "UPDATE job_status SET post_code = '"+truck[1]+"' WHERE indice_id = '"+str(id)+"'"
        mycursor.execute(sql)
        mydb.commit()
    print("Joburile sunt actualizate")
    sql = "UPDATE trucks SET status_cargo = 'Loaded'"
    mycursor.execute(sql)
    mydb.commit()
    print("Status cargo actualizat")

# download_jobs()
file = find_file()
array = read_csv(file)
update_db(array)
