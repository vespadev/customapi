import requests
import json
import mysql.connector
from datetime import date

api_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NjU4NTAsImlhdCI6MTYxNjE0NTk0NSwiZXhwIjoxNjQ3NjgxOTQ1fQ.purWE-tejljfAMH6NIANHdp0rQLXsMKtCDyTecIYzAY"
params = {
    "accept": "application/json",
    "Authorization": 'Bearer ' + api_key
}

def getDriveTime(imei):
    resp = requests.get("https://api.gpslive.app/v1/devices/"+imei+"/today-overviews", headers=params)
    device = resp.json()
    trucks = []
    trucks.append([device['stops_duration'], device['drives_duration'], device['route_length']])
    return trucks

def manage_postcode(addr):
    array = addr.split(',')
    if (len(array) > 3):
        if (array[2] == " IP10 0DD"):
           return 0
        else:
            return 1

# Basic info for each device
def getDevices():
    mydb = mysql.connector.connect(
        host="hosting2166736.online.pro",
        user="00461784_deeatvx",
        password="Pinguin1..",
        database="00461784_deeatvx"
    )
    cursor = mydb.cursor()
    cursor.execute("SELECT driver_id,truck_id FROM job_status")
    res = cursor.fetchall()
    truck = []
    for re in res:
        mycursor = mydb.cursor()
        mycursor.execute("SELECT serial_truck,current_address FROM trucks WHERE id = "+str(re[1]))
        data = mycursor.fetchone()
        night_out = manage_postcode(data[1])
        truck_imei = data[0]
        truck.append([re[1],truck_imei,re[0],night_out])#Truck,Truck_imei,Driver,Post_Code
    return truck

def update_trucks(trucks):
    mydb = mysql.connector.connect(
        host="hosting2166736.online.pro",
        user="00461784_deeatvx",
        password="Pinguin1..",
        database="00461784_deeatvx"
    )
    mycursor = mydb.cursor()
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    name_day = today.strftime("%A")
    if name_day != "Sunday":
        for truck in trucks:
            truckx = getDriveTime(truck[1])
            if checkExistence(truck[0],d1) == 0:
                sql = "INSERT INTO overview (id_truck, id_driver, drives_duration, stops_duration, route_length, created_at, night_out) VALUES (%s, %s, %s, %s, %s, %s,%s)"
                params = (str(truck[0]),truck[2],truckx[0][1],truckx[0][0],truckx[0][2], d1, truck[3])
                mycursor.execute(sql,params)
                mydb.commit()
            else:
                sql = "UPDATE overview Set drives_duration= %s, stops_duration = %s, route_length = %s, night_out = %s WHERE id_driver= %s and created_at = %s"
                params = (truckx[0][1],truckx[0][0],truckx[0][2],truck[3] ,truck[2],d1)
                mycursor.execute(sql,params)
                mydb.commit()

def checkExistence(truck,date):
    mydb = mysql.connector.connect(
            host="hosting2166736.online.pro",
            user="00461784_deeatvx",
            password="Pinguin1..",
            database="00461784_deeatvx"
        )
    mycursor = mydb.cursor()
    sql = """SELECT COUNT(id) FROM overview WHERE created_at = %s and id_truck = %s """
    mycursor.execute(sql,(date,truck))
    res = mycursor.fetchone()
    return res[0]

trucks = getDevices()
update_trucks(trucks)
