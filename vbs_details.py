from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time
import glob
import os
from csv import DictReader
import mysql.connector

def download_jobs():
    chrome_options = webdriver.ChromeOptions();
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_experimental_option("excludeSwitches", ['enable-logging']);
    driver = webdriver.Chrome(ChromeDriverManager().install(),options=chrome_options)
    driver.get("https://1to1transport.xt.truckcom.net/Home/Jobs")
    time.sleep(1)
    email = driver.find_element_by_name('Username')
    email.send_keys("deea")
    passw = driver.find_element_by_name('Password')
    passw.send_keys("welcome1")
    submit = driver.find_element_by_id("LoginButton")
    submit.click()
    time.sleep(2)
    btn0 = driver.find_element_by_xpath("/html/body/div[17]/table/tbody/tr[2]/td/div[1]/table/tbody/tr/td/div/div/div[5]/div/table/tbody/tr[1]/td[5]")
    btn0.click()
    btn1 = driver.find_element_by_xpath("//*[@id='JobsGrid']/div/div[4]/div/div/div[3]/div[4]/div/div")
    btn1.click()
    btn2 = driver.find_element_by_xpath("/html/body/div[25]/div/div/ul/li[2]/div")
    btn2.click()
    btn3 = driver.find_element_by_xpath("/html/body/div[25]/div/div/ul/li[2]/div/div[2]/ul/li[1]/div")
    btn3.click()
    print("Am terminat descarcarea")
    time.sleep(1)
    driver.close()

def find_file():
    list_of_files = glob.glob('C:/Users/Warlow/Downloads/*.csv')
    latest_file = max(list_of_files, key=os.path.getctime)
    print("Am gasit fisierul")
    return latest_file

def read_csv(file,drop):
    with open(file, 'r') as read_obj:
        csv_dict_reader = DictReader(read_obj)
        array = []
        for row in csv_dict_reader:
            name = row['Resource'].split()
            nr = len(name)
            if nr > 2:
                indice = name[0][0] + name[1]
            else:
                indice = name[0][0] + name[0][4] + name[0][5]
            load = row['Stop 1']
            drop = row['Stop 3']
            pin = row['Reference 1']
            container = row['Load Id']
            details = row['Notes'].split()
            vbs_hour = details[0]
            vbs_nr = get_vbs_nr(details)
            if vbs_nr == None:
                vbs_nr = "0000000"
            array.append([indice,load,drop,pin, container,vbs_nr, vbs_hour])
        print("Am citit csv-ul")
        return array

def get_vbs_nr(details):
    for detail in details:
        if len(detail) == 7 and detail[0] == "4":
            return detail

def get_truck(name):
    mydb = mysql.connector.connect(
      host="hosting2166736.online.pro",
      user="00461784_deeatvx",
      password="Pinguin1..",
      database="00461784_deeatvx"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT id FROM indici WHERE indice='"+name+"'")
    myresult = mycursor.fetchone()
    return myresult[0]

def update_db(array):
    mydb = mysql.connector.connect(
      host="hosting2166736.online.pro",
      user="00461784_deeatvx",
      password="Pinguin1..",
      database="00461784_deeatvx"
    )
    mycursor = mydb.cursor()
    sql = "TRUNCATE info_cards"
    mycursor.execute(sql)
    for data in array:
        indice = get_truck(data[0])
        sql = "INSERT INTO info_cards (indice_id, container_load, container_drop, pin, container, vbs_nr,details)"
        params = "VALUES (%s,%s,%s,%s,%s,%s,%s)"
        truple = (indice, data[1],data[2],data[3],data[4],data[5], data[6])
        sql = sql+params
        mycursor.execute(sql,truple)
        mydb.commit()
download_jobs()
file = find_file()
array = read_csv(file)
update_db(array)
