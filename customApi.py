from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time
import glob
import os
from csv import DictReader
import mysql.connector
import pickle

# Start chrome scrapping
def run_chrome(addr1,addr2):
    driver.get("https://www.google.com/maps?saddr="+addr1+"&daddr="+addr2)
    time.sleep(1)
    try:
        print("gasit")
    except:
        pass

# GET ETAs from google by scrapping
def request_data(jobs):
    remake = []
    for job in jobs:
        print(job)
        addr2 = job[3].strip()
        addr1 = str(job[1])+","+str(job[2])
        run_chrome(addr2,addr1)
        eta = get_eta()
        job = list(job)
        job.append(eta)
        remake.append(job)
    return remake

# GET ETA by scrapping.
def get_eta():
    time.sleep(1)
    try:
        eta_hours = driver.find_element(By.CSS_SELECTOR,'.Fk3sm > span').text
        eta_distance = driver.find_element(By.CSS_SELECTOR,'.ivN21e > div').text
        eta = [eta_hours,eta_distance]
    except:
        eta = ["Reloading","Reloading"]
    return eta

# Query DB to get truck_id, addr,dest.
def get_data_from_db():
    mydb = mysql.connector.connect(
        host="89.46.7.25",
        user="vespadev_root",
        password="Pinguin1..",
        database="vespadev_deeatvx"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT trucks.id,lat,lng,post_code FROM trucks, job_status WHERE trucks.id=job_status.truck_id")
    myresult = mycursor.fetchall()
    return myresult

# Update ETA for free
def insert_into_db(trucks):
    mydb = mysql.connector.connect(
        host="89.46.7.25",
        user="vespadev_root",
        password="Pinguin1..",
        database="vespadev_deeatvx"
    )
    mycursor = mydb.cursor()
    for truck in trucks:
        mycursor = mydb.cursor()
        truck[4][1] = truck[4][1].replace("milesn","miles")
        try:
            sql = "UPDATE eta SET eta_destination = '"+str(truck[4][1])+"',eta_duration = '"+str(truck[4][0])+"' WHERE truck_id = '"+str(truck[0])+"'"
            mycursor.execute(sql)
            mydb.commit()
        except:
            print(truck)

chrome_options = Options();
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument("--lang=en-GB")
chrome_options.add_argument("--headless")
chrome_options.add_experimental_option("excludeSwitches", ['enable-logging']);
driver = webdriver.Chrome(ChromeDriverManager().install(),options=chrome_options)
driver.get("http://www.google.com")
cookies = pickle.load(open("cookies.pkl", "rb"))
for cookie in cookies:
    driver.add_cookie(cookie)
# Start Scrapping & Updateing db.
data = get_data_from_db()
data = request_data(data)
insert_into_db(data)
print("Gata")
driver.close()
driver.quit()
